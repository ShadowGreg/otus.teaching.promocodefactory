﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models; 

public class CreateOrEditEmployeesRequest {
    public string Email { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public int AppliedPromocodesCount { get; set; }


}